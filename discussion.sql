-- List down all the databases inside the DBMS
SHOW DATABASES;
-- Create a database.
-- Syntax:
    --  CREATE DATABASE <name_db>

CREATE DATABASE music_db;

--  DROP DATABSE, deletion of database
-- Syntax:
    -- DROP DATABASE <name_db>
DROP DATABASE music_db;

-- SELECT a database.
-- Syntax
    -- USE <name_db>

-- CREATE tables.
-- Table colums will also be declared here

CREATE TABLE users(
    -- INT to indicate that the id column will have integer entry
    id INT NOT NULL AUTO_INCREMENT,
    -- VARCHAR like string
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number CHAR(11) NOT NULL,
    email VARCHAR(50) NOT NULL, 
    address VARCHAR(50),
    -- To set the primary key
    PRIMARY KEY(id)
    );


-- Drop table 
DROP TABLE userss;

CREATE TABLE playlists(
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    playlist_name VARCHAR(30) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT

);

-- artist table
CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    datetime_released DATETIME NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_albums_artist_id
        FOREIGN KEY (artist_id) REFERENCES artists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE songs(
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_album_id
        FOREIGN KEY (album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists_songs(
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_playlist_id
        FOREIGN KEY (playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_song_id
        FOREIGN KEY (song_id) REFERENCES songs (id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);